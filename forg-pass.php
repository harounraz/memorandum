<?php
	session_start();
	$userid = $_SESSION['userid'];
	$username = $_SESSION['username'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>IUCO - Memorandum</title>
	<link rel="stylesheet" media="screen" href="css/forg.css" >
</head>
<body>
<form class="contact_form" action="forget-pass.php" method="post" name="contact_form">
	<center><div style="margin: 6% 0 0 0; font: 30px Trebuchet MS; color: #2a6da9;">
	<img src="images/syslogo.gif">IUCO Memorandum System</div></center>
	<center><div style="margin: 10px 0 0 -350px; font: 13px Trebuchet MS; color: #2a6da9;"><a href="index.php">Home </a> >> Forget Password</div></center>
    <ul>
        <li>
        <label>Username:</label>
			<input type="text"  placeholder="user.title" required  name="username"/>
			<span class="form_hint">Proper format "user.title"</span>
        </li>
		<li>
        <label>Email:</label> 
			<input type="email"  placeholder="pat@example.com" required name="email" style="margin: 20px 0 0 47px;"/>
			<span class="form_hint">Proper format "pat@example.com"</span>
        </li>
        <li>
        	<button class="submit" type="submit" style="margin: 0 0 0 78px;">Reset Password</button>
        </li>
    </ul>

</form>
</body>
</html>
