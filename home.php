<?php
	session_start();
	$userid = $_SESSION['userid'];
	$username = $_SESSION['username'];
		
		include("includes/conn.php");
		$query = mysql_query("SELECT * FROM login WHERE username='$username'");
		$row = mysql_fetch_array($query);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>IUCO - Memorandum :: <?php echo $row['position']; ?></title>
	<link rel="stylesheet" media="screen" href="css/styles.css" >
</head>
<body>
	<?php 
		include("sess.php");
	?>
<table align="center">
		<tr>
			<td><a href="home.php">Home</a></td>
			<td><a href="find.php">Search</a></td>
			<td><a href="view.php">View Data</a></td>
			<td><a href="change-pass.php">Change Password</a></td>
		</tr>
</table>
<form class="contact_form" action="record.php" method="post" name="contact_form">
	<center><div style="margin: 2% 0 0 0; font: 20px Trebuchet MS; color: #2a6da9;">IUCO Memorandum System :: <?php echo $row['position']; ?></div></center>
    <div style="margin: -70px 0 0 0;">
	<ul>
    <li>
        <h2><font color="#2a6da9">Register User</font></h2>
		<span class="required_notification"><?php echo $row['fname']; ?>&nbsp;&nbsp;<?php echo $row['lname']; ?>
		|<a href="logout.php">Logout</a></span>
    </li>
    <li>
        <label for="name">First Name:</label>
        <input type="text"  placeholder="Zacharia" required  name="fname"/>
	    <span class="form_hint">Proper format "Zacharia"</span>
    </li>
	<li>
        <label for="name">Last Name:</label>
        <input type="text"  placeholder="Haule" required name="lname"/>
	    <span class="form_hint">Proper format "Haule"</span>
    </li>
	<li>
        <label for="name">Username:</label>
        <input type="text"  placeholder="user.provost" required name="username"/>
	    <span class="form_hint">Proper format "user.provost"</span>
    </li>
    <li>
        <label for="name">Email:</label>
        <input type="email"  placeholder="pat@example.com" required name="email" />
	    <span class="form_hint">Proper format "pat@example.com"</span>
    </li>
	<li>
        <label for="name">Position:</label>
        <select name="position">
		<option>-----Select position-----</option>
		<option value="Administrator">Administrator</option>
		<option value="Provost">Provost</option>
		<option value="DPAA">DPAA</option>
		<option value="DPA">DPA</option>
		<option value="CFO">CFO</option>
		<option value="Dean Students">Dean Students</option>
		<option value="Warden">Warden</option>
		<option value="Dean FASS">Dean FASS</option>
		<option value="Dean Theology">Dean Theology</option>
		<option value="Dean FABEC">Dean FABEC</option>
		<option value="Dean LLB">Dean LLB</option>
		<option value="Dean FASE">Dean FASE</option>
		<option value="Assoc. Dean FASS">Assoc. Dean FASS</option>
		<option value="Assoc. Dean Theology">Assoc. Dean Theology</option>
		<option value="Assoc. Dean FABEC">Assoc. Dean FABEC</option>
		<option value="Assoc. Dean LLB">Assoc. Dean LLB</option>
		<option value="Assoc. Dean FASE">Assoc. Dean FASE</option>
		<option value="Dir. Postgraduate Studies">Dir. Postgraduate Studies</option>
		<option value="Dir. Consultancy Bureau">Dir. Consultancy Bureau</option>
		<option value="Dir. Library & Information Services">Dir. Library & Information Services</option>
		<option value="Dir. ICT">Dir. ICT</option>
		</select>   
    </li>
	<li>
        <label for="name">Gender:</label>
		<select name="gender">
		<option>-----Select gender-----</option>
		<option value="Female">Female</option>
		<option value="Male">Male</option>
		</select> 
    </li>
	<li>
        <label for="name">Status:</label>
		<select name="status">
		<option>-----Select status-----</option>
		<option value="Admin">Administrator</option>
		<option value="User">User</option>
		</select> 
    </li>
    <li>
        	<button class="submit" type="submit">Register</button>
    </li>
    </ul>
	</div>
</form>

</body>
</html>
