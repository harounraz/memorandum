<?php 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Memorandum</title>
	<link rel="stylesheet" media="screen" href="css/login.css" >
</head>
<body>
<form class="contact_form" action="login.php" method="post" name="contact_form">
	<center><div style="margin: 6% 0 0 0; font: 30px Trebuchet MS; color: #2a6da9;">
	<img src="images/syslogo.gif">IUCO Memorandum System</div></center>
	<center><div style="margin: 10px 0 0 -375px; font: 13px Trebuchet MS; color: #2a6da9;">You are here >> <a href="index.php">Login</a></div></center>
    <ul>
        <li>
        <label>Username:</label>
			<input type="text"  placeholder="user.title" required  name="username"/>
			<span class="form_hint">Proper format "user.title"</span>
        </li>
		<li>
        <label>Password:</label> 
			<input type="password"  placeholder="xxxxxxx" required name="password" style="margin: 20px 0 0 25px;"/>
			<span class="form_hint">Proper format "xxxxxxx"</span>
        </li>
        <li>
        	<button class="submit" type="submit" style="margin: 0 0 0 80px;">Login</button>
        </li>
		<a style="margin: 0 0 0 92px; color: #000;" href="forg-pass.php">Forgot password</a>	
    </ul>
</form>
</body>
</html>
