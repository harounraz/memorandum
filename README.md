Memorandum
==========
A system designed to assist schools and even offices in electronically certifying that the required actions and activities have been completed. The Memorandum system users are able to write a short memo to other members of the same institution and even attach a report sending to other users of the system.

Quick Tutorial
==============
Download a .zip package and extract it to your web server root directory. Import the database named "memorandum". Use the below credentials to access the system;

Admin Login
===========
	Username: admin
	Password: admin

User1 Login
===========
  	Username: user.provost
	Password: provost

User2 Login
===========
  	Username: user.dpaa
	Password: dpaa
