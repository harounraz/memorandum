<?php
	session_start();
	$userid = $_SESSION['userid'];
	$username = $_SESSION['username'];
	
	include("includes/conn.php");
	
	if (isset($_GET['id']) && is_numeric($_GET['id'])) {
		$id = $_GET['id'];;
	}
	
	$edit = mysql_query("SELECT * FROM login WHERE id ='$id'");
	while($row = mysql_fetch_array($edit)){
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		$user = $_SESSION['username'];
		$query = mysql_query("SELECT * FROM login WHERE username='$user'");
		$show = mysql_fetch_array($query);
	?>
	<meta charset="utf-8">
	<title>IUCO - Memorandum :: <?php echo $show['position']; ?></title>
	<link rel="stylesheet" media="screen" href="css/styles.css" >
</head>
<body>
	<?php include("sess.php"); ?>
<table align="center">
		<tr>
			<td><a href="home.php">Home</a></td>
			<td><a href="find.php">Search</a></td>
			<td><a href="view.php">View Data</a></td>
			<td><a href="change-pass.php">Change Password</a></td>
		</tr>
</table>
<form class="contact_form" action="update.php" method="post" name="contact_form">
	<center><div style="margin: 2% 0 0 0; font: 20px Trebuchet MS; color: #2a6da9;">
	IUCO Memorandum System :: <?php echo $show['position']; ?></div></center>
    <div style="margin: -70px 0 0 0;">
	<input type="hidden" name="id" value ="<?php echo $id; ?>">
    <ul>
    <li>
        <h2><font color="#2a6da9">Register User</font></h2>
		<span class="required_notification"><?php echo $show['fname']; ?>&nbsp;&nbsp;<?php echo $show['lname']; ?>
		|<a href="logout.php">Logout</a></span>
    </li>
	<li>
        <label for="name">First Name:</label>
        <input type="text"  placeholder="Zacharia" required  name="fname" value="<?php echo $row['fname']; ?>"/>
	    <span class="form_hint">Proper format "Zacharia"</span>
    </li>
	<li>
        <label for="name">Last Name:</label>
        <input type="text"  placeholder="Haule" required name="lname" value="<?php echo $row['lname']; ?>" />
	    <span class="form_hint">Proper format "Haule"</span>
    </li>
	<li>
        <label for="name">Username:</label>
        <input type="text"  placeholder="user.provost" required name="username" value="<?php echo $row['username']; ?>" />
	    <span class="form_hint">Proper format "user.provost"</span>
    </li>
    <li>
        <label for="name">Email:</label>
        <input type="email"  placeholder="pat@example.com" required name="email" value="<?php echo $row['email']; ?>" />
	    <span class="form_hint">Proper format "pat@example.com"</span>
    </li>
	<li>
        <label for="name">Active:</label>
        <input type="boolean"  placeholder="0 or 1" required name="active" value="<?php echo $row['active']; ?>" />
	    <span class="form_hint">Proper format "0 or 1"</span>
    </li>
	<li>
        <label for="name">Position:</label>
        <select name="position">
		<option selected="selected"><?php echo $row['position']; ?></option>
		<option value="Provost">Provost</option>
		<option value="DPAA">DPAA</option>
		<option value="DPA">DPA</option>
		<option value="CFO">CFO</option>
		<option value="Dean Students">Dean Students</option>
		<option value="Warden">Warden</option>
		<option value="Dean FASS">Dean FASS</option>
		<option value="Dean Theology">Dean Theology</option>
		<option value="Dean FABEC">Dean FABEC</option>
		<option value="Dean LLB">Dean LLB</option>
		<option value="Dean FASE">Dean FASE</option>
		<option value="Assoc. Dean FASS">Assoc. Dean FASS</option>
		<option value="Assoc. Dean Theology">Assoc. Dean Theology</option>
		<option value="Assoc. Dean FABEC">Assoc. Dean FABEC</option>
		<option value="Assoc. Dean LLB">Assoc. Dean LLB</option>
		<option value="Assoc. Dean FASE">Assoc. Dean FASE</option>
		<option value="Dir. Postgraduate Studies">Dir. Postgraduate Studies</option>
		<option value="Dir. Consultancy Bureau">Dir. Consultancy Bureau</option>
		<option value="Dir. Library & Information Services">Dir. Library & Information Services</option>
		<option value="Dir. ICT">Dir. ICT</option>
		</select>   
    </li>
	<li>
        <label for="name">Gender:</label>
		<select name="gender">
		<option selected="selected"><?php echo $row['gender']; ?></option>
		<option value="Female">Female</option>
		<option value="Male">Male</option>
		</select> 
    </li>
	<li>
        <label for="name">Status:</label>
		<select name="status">
		<option selected="selected"><?php echo $row['status']; ?></option>
		<option value="Admin">Administrator</option>
		<option value="User">User</option>
		</select> 
    </li>
    <li>
        	<button class="submit" type="submit">Update</button>
    </li>
    </ul>
	</div>
</form>
<?php
}
?>
</body>
</html>
