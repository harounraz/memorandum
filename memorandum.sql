/*

SQLyog Ultimate v8.55 
MySQL - 5.6.16 : Database - memorandum

*********************************************************************

*/



/*!40101 SET NAMES utf8 */;



/*!40101 SET SQL_MODE=''*/;



/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`memorandum` /*!40100 DEFAULT CHARACTER SET latin1 */;



USE `memorandum`;



/*Table structure for table `login` */



DROP TABLE IF EXISTS `login`;



CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) DEFAULT NULL,
  `lname` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;



/*Data for the table `login` */



insert  into `login`(`id`,`fname`,`lname`,`username`,`password`,`email`,`position`,`gender`,`active`,`status`) values (1,'Haroun','Majaga','admin','21232f297a57a5a743894a0e4a801fc3','admin@admin.com','Administrator','Male',1,'Admin'),(4,'Professor','Bangu','user.provost','fdd38e8fec9e944c91617448eadce2c1','bangu@provost.com','Provost','Male',1,'User'),(5,'Dr.','Lubawa','user.dpaa','d11baf209790fd9ea5e45ea9e750bb3d','lubawa@dpaa.com','DPAA','Male',0,'User');



/*Table structure for table `message` */



DROP TABLE IF EXISTS `message`;



CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(250) DEFAULT NULL,
  `recipient` varchar(250) DEFAULT NULL,
  `contents` text,
  `status` varchar(100) DEFAULT NULL,
  `date_created` varchar(250) DEFAULT NULL,
  `image_path` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;



/*Data for the table `message` */



insert  into `message`(`id`,`sender`,`recipient`,`contents`,`status`,`date_created`,`image_path`) values (35,'user.dpaa','user.provost','Habari za hapo mkuu<br>','read','03/22/2014 13:12:45','uploads/Advance Website.docx'),(39,'user.dpaa','user.provost','Habari ya mchana<br>','read','03/22/2014 17:33:41','uploads/'),(40,'user.provost','user.dpaa','Hallo DPAA..??<br>','read','03/22/2014 18:17:09','uploads/'),(41,'user.provost','user.dpaa','Check out<br>','read','03/22/2014 19:34:29','uploads/com_jefs.zip');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
